package Teste3;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

 //MyFirstRobot - a robot by (Luan Luiz Teixeira Da SIlva)
 
public class DemolidorBaratinha extends AdvancedRobot 
{
	//método que inicia o robô
	public void run() {
		
		//define a cor do corpo
		setBodyColor(Color.red); 
		//define a cor do canhão
		setGunColor(Color.yellow);        
		//define a cor do radar
		setRadarColor(Color.black); 
		//define a cor do scanner
		setScanColor(Color.black);
		//define a cor do tiro       
		setBulletColor(Color.yellow);
		
 		//faz a movimentação continua em zig-zag e verifica se o radar encontrou algum inimigo
		while(true) {
			ahead(50);
			setTurnRight(180);
			ahead(50);
			setTurnLeft(180);
			execute();
			scan();
		}
	}
	//método chamado quando o radar localiza algum inimigo
	public void onScannedRobot(ScannedRobotEvent e) {
		//armaze a distancia e a posição do inimigo dentro das variáveis de ponto flutuante
		double distanciaInimigo = e.getDistance();
		double posicaoInimigo = e.getBearing();
		//verifica a distancia e calibra a potência do disparo
		if (distanciaInimigo >= 200) {
			fire(1);
			//depois do disparo avança na direção do inimigo
			for(int i = 0; i <= 3; i++) {
				turnRight(posicaoInimigo);
				setAhead(100);
				execute();
			}
		//verifica a distancia e calibra a potência do disparo
		} else if (distanciaInimigo >= 100) {
			fire(2);
			//depois do disparo avança na direção do inimigo
			for(int i = 0; i <= 3; i++) {
				turnRight(posicaoInimigo);
				setAhead(100);
				execute();
			}
		//caso a distancia seja menor que 100 ele executa o bloco de código seguinte
		} else {
			fire(3);
			//depois do disparo avança na direção do inimigo
			for(int i = 0; i <= 3; i++) {
				turnRight(posicaoInimigo);
				setAhead(100);
				execute();
			}
		}
		//verifica se o radar encontrou algum inimigo
		scan();
	}
	//método chamado quando o robô é atingido por um disparo
	public void onHitByBullet(HitByBulletEvent e) {
		//volta 100pixels girando 90 graus a direita
		back(100);
		turnRight(90);
	}
	//método chamado quando o robô bate na parede
	public void onHitWall(HitWallEvent e) {
		//volta 100pixels girando 90 graus a direita
		back(100);
		turnRight(90);
	}
	//método chamado quando o robô bate em um inimigo ou vice-versa
	public void onHitRobot (HitRobotEvent e) {
		//gira para direita na posição do inimigo e dispara com potência 3
		turnRight(e.getBearing());
		fire(3);
		//volta 150 pixels girando 90 graus a direita
		setBack(150);
		setTurnRight(90);
		execute();
	}
}