# Solutis Robot
Robô criado em Java no Robocode para a Solutis Robot Arena.
## Demolidor Baratinha
### Linguagem
O robô demolidor baratinha foi desenvolvido utilizando java no editor do próprio Robocode.
### Movimetação
Ele se movimenta pelo campo de batalha sempre em zig-zag em busca de robôs inimigos.
### Ações
Ao localizar um robô inimigo através do `scan()`, ele obtém a distância do inimigo e sua posição, realiza uma verificação e decide a potêcia na qual vai ser o disparo do canhão e posiciona o canhão de acordo com a posição obtida.
&nbsp;

Quando atingido por um disparo de outro robô ou atingir uma das quatro paredes ele volta alguns pixels fazendo um curva para a direita.
&nbsp;

Caso o robô chegue a colidir com um inimigo, é obtido a posição dele, feito um giro para sua posição e efetuado um dispoaro com `fire(3)`. Logo em seguida o robô afesta-se do inimigo e continua sua movimentação padrão.

### Ponto fraco e forte

**Forte**: Verifica a distância do inimigo e efetua disparos com maior potêcia somente quando tem maior precisão(curta distância).
&nbsp;

**Fraco**: Perde vida quando se choca com a parede ou um inimigo.
